#!/bin/sh
#SBATCH --partition=debug_latest
#SBATCH --job-name=GromacsMD
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=18
#SBATCH --cpus-per-task=1
#SBATCH --time=00-00:60:00
#SBATCH -o %j.o
#SBATCH -e %j.e
#SBATCH --constraint=op

# GROMACS STEPS:
# 1 Generate Topology
# 2 Define Unit Cell and Solvate
# 3 Neutralise the system
# 4 Energy Minimisation
# 5 NVT Equilibration
# 6 NPT Equilibration
# 7 Production MD
# 8 Post-MD Analysis
# 9 Rendering / Animation

# source external scripts
source $MWSHPATH/gmx_funcs.sh   # functions for running Gromacs
source $MWSHPATH/load_gmx.sh    # load necessary modules and source GMXRC.sh (Gromacs Initialisation)

######################################################################################################################

CLEAN_ONLY=0
STOP_AFTER=99
START_FROM=0
THIS_SECTION=-1

RUN_AS=$@

while test $# -gt 0; do
  case "$1" in
    -gg|--genion-group)
      shift
      GENION_GROUP=$1
      shift
      ;;
    -co|--clean-only)
      shift
      CLEAN_ONLY=1
      ;;
    -sa|--stop-after)
      shift
      STOP_AFTER=$1
      shift
      ;;
    -sf|--start-from)
      shift
      START_FROM=$1
      shift
      ;;
    -d|--data)
      shift
      DATA=$DATA" "$1
      shift
      ;;
    *)
      echo -e $colError"Unknown flag: "$colArg$1$colClear
      exit 4
      ;;
  esac
done

######################################################################################################################

# prepare the directory
if [[ -z ${SLURM_JOBID} ]] ; then
  # if not submitted with SLURM, clean directory:
  if [ $CLEAN_ONLY -eq 1 ] ; then     
    rm -f \#*
    rm -f 1aki_*.gro
    rm -f 1aki_noHOH.pdb
    rm -f topol.top
    rm -f posre.itp
    rm -f ions.tpr
    rm -f _*.log
    rm -f mdout.mdp
    rm -f *.xvg
    rm -f *.png
    rm -f em.*
    # if only cleaning, exit
    exit 0
  fi
else
  # log output
  echo ${SLURM_JOBID}": "run_gmx.sh" "$RUN_AS" [ started @ $(date) ]" >> run_log
  echo ${SLURM_JOBID} > last_job
  
  # set paths
  PSCRATCH=$HOME/parallel_scratch
  SCRATCH_DIR=$PSCRATCH/${SLURM_JOBID}
  WORK_DIR=${SLURM_SUBMIT_DIR}

  # prepare the scratch directory
  mkdir $SCRATCH_DIR

  if [[ ! -z $DATA ]] ; then
    # copy contents of data directories
    for FOLDER in $DATA; do
      cp -r $FOLDER/* $SCRATCH_DIR
    done
  fi

  cp $WORK_DIR/* $SCRATCH_DIR/ 2> /dev/null

  cd $SCRATCH_DIR
  rm *.e *.o
  rm run_log
  rm last_job
  rm README.md
fi

##### 1 Generate Topology

if breakCheck 1 $START_FROM $STOP_AFTER; then 
  fancyOut "(1) Generate Topology"

  removeWater 1aki.pdb 1aki_noHOH.pdb
  gmx_max pdb2gmx 1 -f 1aki_noHOH.pdb -o 1aki_processed.gro -ff oplsaa -water spce
  totalCharge topol.top
fi

##### 2 Define Unit Cell and Solvate

if breakCheck 2 $START_FROM $STOP_AFTER; then 
  fancyOut "(2) Define Unit Cell and Solvate"

  gmx_max editconf 1 -f 1aki_processed.gro -o 1aki_newbox.gro -c -d 1.0 -bt cubic
  gmx_max solvate 1 -cp 1aki_newbox.gro -cs spc216.gro -o 1aki_solv.gro -p topol.top
fi

##### 3 Neutralise the system

if breakCheck 3 $START_FROM $STOP_AFTER; then 
  fancyOut "(3) Neutralise the system"

  gmx_max grompp 1 -f ions.mdp -c 1aki_solv.gro -p topol.top -o ions.tpr
  gmx_max genion 1 -s ions.tpr -o 1aki_solv_ions.gro -p topol.top -pname NA -nname CL -neutral
fi

##### 4 Energy Minimisation

if breakCheck 4 $START_FROM $STOP_AFTER; then 
  fancyOut "(4) Energy Minimisation"

  gmx_max grompp 2 -f minim_100.mdp -c 1aki_solv_ions.gro -p topol.top -o em.tpr
  gmx_max mdrun 1 -v -deffnm em
  minimStats mdrun 1

  gmx_max energy 1 10 -f em.edr -o potential.xvg
  xvg2png 1 -f potential -xl "Energy Minimisation Steps" -yl "Potential Energy kJ/mol" -ys
fi

##### 5 NVT Equilibration

if breakCheck 5 $START_FROM $STOP_AFTER; then 
  fancyOut "(5) NVT Equilibration"

  gmx_max grompp 3 -f nvt.mdp -c em.gro -r em.gro -p topol.top -o nvt.tpr
  gmx_max mdrun 2 -deffnm nvt

  gmx_max energy 2 16 -f nvt.edr -o temperature.xvg
  xvg2png 2 -f temperature -xl "time [ps]" -yl "Temperature [K]" --constfit -ytic
fi

##### 6 NPT Equilibration

if breakCheck 6 $START_FROM $STOP_AFTER; then 
  fancyOut "(6) NPT Equilibration"

  gmx_max grompp 4 -f npt.mdp -c nvt.gro -r nvt.gro -t nvt.cpt -p topol.top -o npt.tpr
  gmx_max mdrun 3 -deffnm npt

  gmx_max energy 3 18 -f npt.edr -o pressure.xvg
  xvg2png 3 -f pressure -xl "time [ps]" -yl "pressure [bar]" -ra -ymin -500 -ymax 500 -yt 250

  gmx_max energy 3 18 -f npt.edr -o pressure.xvg
  xvg2png 3 -f pressure -xl "time [ps]" -yl "pressure [bar]" -o "pressure_fit" -ymin -500 -ymax 500 -yt 250 --constfit -fmin 5

  gmx_max energy 4 24 -f npt.edr -o density.xvg
  xvg2png 4 -f density -xl "time [ps]" -yl "density [kg/m^3]" -ymin 1000 -ymax 1030 --constfit
fi

##### 7 Production MD

if breakCheck 7 $START_FROM $STOP_AFTER; then 
  fancyOut "(7) Production MD"

  gmx_max grompp 5 -f md.mdp -c npt.gro -t npt.cpt -p topol.top -o md_0_1.tpr
  echo $(grep "computational load" _grompp5.log)
  echo $(grep "This run will generate" _grompp5.log)
  gmx_max mdrun 4 -deffnm md_0_1
fi

##### 8 Post-MD Analysis

if breakCheck 8 $START_FROM $STOP_AFTER; then 
  fancyOut "(8) Post-MD Analysis"

  # Center the system (0) on the CoM of the protein (1)
  gmx_max trjconv 1 1 -s md_0_1.tpr -f md_0_1.xtc -o md_0_1_noPBC.xtc -pbc mol -center
  gmx_max rms 1 4 -s md_0_1.tpr -f md_0_1_noPBC.xtc -o rmsd.xvg -tu ns
  gmx_max rms 2 4 -s em.tpr -f md_0_1_noPBC.xtc -o rmsd_xtal.xvg -tu ns

  xvg2png 5 -dp -f1 rmsd -f2 rmsd_xtal -t1 "Ref:  Equilibrated" -t2 "Ref: Crystal" -xl 'Time [ns]' -yl 'RMSD (backbone) [nm]'

  gmx_max gyrate 1 1 -s md_0_1.tpr -f md_0_1_noPBC.xtc -o gyrate.xvg
  xvg2png 6 -f gyrate -xl 'Time [ps]' -yl 'R_g [nm]' -ymin 1.3 -ymax 1.5
fi

##### 9 Rendering / Animation

if breakCheck 9 $START_FROM $STOP_AFTER; then 
  fancyOut "(9) Rendering / Animation"

  # gmx_max trjconv 2 1 -s md_0_1.tpr -f md_0_1.xtc -o md_0_1.gro
  gmx_max trjconv 2 1 -s md_0_1.tpr -f md_0_1_noPBC.xtc -o md_0_1.pdb

  module purge
  module load anaconda3/2019.03
  python3 ase_anim.py md_0_1.pdb pov4 1 # 2> _aseamp1.log

fi

######################################################################################################################

finishUp 0