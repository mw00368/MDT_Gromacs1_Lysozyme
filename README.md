# MDT_Gromacs1_Lysozyme

Instructions and files to complete "Tutorial 1: Lysozyme in Water" from [mdtutorials.com](#http://www.mdtutorials.com/gmx/lysozyme/01_pdb2gmx.html).

## Instructions

**1.  Clone this directory `git clone https://gitlab.eps.surrey.ac.uk/mw00368/MDT_Gromacs1_Lysozyme.git` to your EUREKA home directory (SSH into EUREKA, `cd ~` and run the above `git clone` command).**

**2.  Load the GROMACS module, and source GMXRC.bash. This can be achieved by sourcing the included bash script:**
    
`source load_gmx.sh`

**3.  Remove water from from PDB file:**

`grep -v HOH 1aki.pdb > 1aki_noHOH.pdb`

`grep` searches a given file for the given 'pattern'. In this case "HOH" is the pattern and "1aki.pdb" is the file. the `-v` flag tells `grep` to output all non-matching lines. This output is redirected using `>` (BASH redirect) to the file "1aki_noHOH.pdb". The PDB was obtained [here](#https://www.rcsb.org/structure/1AKI).

**4. Run `pdb2gmx` to produce the GROMACS input files:**

`gmx_mpi pdb2gmx -f 1aki_noHOH.pdb -o 1aki_processed.gro -ff oplsaa -water spce`

* `-f <FILE>` specifies the input PDB
* `-o <FILE>` specifies the output GROMACS file
* `-ff <FF_NAME>` select a forcefield
* `-water <WATER>` select the water model

`pdb2gmx` will produce the following files:

* *1aki_processed.gro*    GROMACS formatted structure file.
* *topol.top*             System topology.
* *posre.itp*             Restrain the position of heavy atoms.

**5. Inspect the topology file in a text editor. See [mdtutorials.com](#http://www.mdtutorials.com/gmx/lysozyme/02_topology.html) for details.**

**6. Define the unit cell/box dimensions:** 

`gmx_mpi editconf -f 1aki_processed.gro -o 1aki_newbox.gro -c -d 1.0 -bt cubic`

* `-f <FILE>` specifies the input GROMACS file
* `-o <FILE>` specifies the output GROMACS file
* `-c` centers the protein in the unit cell
* `-d <DIST>` sets the distance to the cell edge in nanometres
* `-bt <BOX_TYPE>` sets the unit cell type

**7. Fill the box with solvent:**

`gmx_mpi solvate -cp 1aki_newbox.gro -cs spc216.gro -o 1aki_solv.gro -p topol.top`

* `-cp <FILE>` specifies the unsolvated input structure file
* `-cs <FILE>` specifies the solvent model
* `-o <FILE>` specifies the output  
* `-p <FILE>` specifies the topology  

**8. Neutralise the System**

Assemble the atomic level input (.tpr):

`gmx_mpi grompp -f ions.mdp -c 1aki_solv.gro -p topol.top -o ions.tpr`

* `-f <FILE>` specifies the input molecular dynamics parameter file
* `-c <FILE>` specifies the solvated input structure file
* `-p <FILE>` specifies the topology file
* `-o <FILE>` specifies the output  

Replace enough solvent atoms to neutralise the system:

`gmx_mpi genion -s ions.tpr -o 1aki_solv_ions.gro -p topol.top -pname NA -nname CL -neutral`

* `-s <FILE>` specifies the structure/state file
* `-o <FILE>` specifies the output file
* `-p <FILE>` specifies the topology file
* `-pname <FILE>` select the positive ions to be added
* `-nname <FILE>` select the negative ions to be added
* `-neutral` tell genion to neutralise the system

**9. Energy Minimisation**

Assemble the binary input:

`gmx_mpi grompp -f minim.mdp -c 1aki_solv_ions.gro -p topol.top -o em.tpr`

* `-f <FILE>` specifies the input molecular dynamics parameter file
* `-c <FILE>` specifies the solvated, neutralised input structure file
* `-p <FILE>` specifies the topology file
* `-o <FILE>` specifies the output

Run the energy minimisation:

`gmx_mpi mdrun -v -deffnm em`

* `-v` verbose output
* `-deffnm <NAME>` name for output files, will produce:
    * *<NAME>.log* text log of EM process
    * *<NAME>.edr* binary energy file
    * *<NAME>.trr* binary trajectory
    * *<NAME>.gro* energy minimised structure

Analyse the potential energy:

`gmx_mpi energy -f em.edr -o potential.xvg`

* `-f <FILE>` specifies the input binary energy file
* `-o <FILE>` specifies the output

To produce a .png file from the .xvg above:

`gnuplot -e "filename='potential';xlab='steps';ylab='potential';xtic=300;ysci=1" xvg2png.gp`

* `-e "<VAR_NAME>=<VALUE>"` passes the value <VALUE> as the variable <VAR_NAME>
* `xvg2png.gp` is the gnuplot script
<!-- * run the following command to see all usage: `grep \#? xvg2png.gp | cut -c 4-` -->

**10. Equilibration**

The first step is a NVT Equilibration - which involves a constant Number of particles, Volume and Temperature. This is a computationally expensive step and should be executed on a debug node at the very least. Ideally submitted via SLURM.

Set up and run the MD:

* `gmx_mpi grompp -f nvt.mdp -c em.gro -r em.gro -p topol.top -o nvt.tpr`
* `gmx_mpi mdrun -deffnm nvt`

Analyse the temperature:

* `gmx_mpi energy -f nvt.edr -o temperature.xvg`
* `gnuplot -e "filename='temperature';xlab='steps';ylab='temperature'" xvg2png.gp`

Following the temperature stabilisation, the pressure needs to be stabilised. This is achieved with a NPT equilibration phase.

* `gmx_mpi grompp -f npt.mdp -c nvt.gro -r nvt.gro -t nvt.cpt -p topol.top -o npt.tpr`
* `gmx_mpi mdrun -deffnm npt`
* `gmx_mpi energy -f npt.edr -o pressure.xvg` (18 0, 24 0)

**11. Production MD**

`gmx_mpi grompp -f md.mdp -c npt.gro -t npt.cpt -p topol.top -o md_0_1.tpr`

grompp will print an estimate of the PME load. Which is the ratio of processors dedicated to the PME calculations compared to the PP calculations.

Optimal PME values:

* Cubic unit cell: 0.25 (3 PP : 1 PME)
* Dodecahedral unit cell: 0.33 (2 PP : 1 PME)

**Do not run the following on a login node!**

`gmx_mpi mdrun -deffnm md_0_1`

Took around ~15 minutes on 18 EUREKA cores (debug_latest/node41).

**12. Analysis**

To account for periodicity (protein clipping through the edge of the unit cell):

`gmx_mpi trjconv -s md_0_1.tpr -f md_0_1.xtc -o md_0_1_noPBC.xtc -pbc mol -center`

*   `-s` Structure input
*   `-f` Trajectory input
*   `-o` Output file
*   `-pbc` PBC treatment

*Structural Stability*

Calculate RMSD

`gmx_mpi rms -s md_0_1.tpr -f md_0_1_noPBC.xtc -o rmsd.xvg -tu ns`

Relative to crystal structure:

`gmx_mpi rms -s em.tpr -f md_0_1_noPBC.xtc -o rmsd_xtal.xvg -tu ns`

*Radius of gyration*

`gmx_mpi gyrate -s md_0_1.tpr -f md_0_1_noPBC.xtc -o gyrate.xvg`

## Using the *run_gmx.sh* script:

Alternatively to following the long list of instructions a BASH script that runs all the necessary commands is included. Simply clone this repository onto EUREKA, go onto a debug node `ssh node41`, change to the directory `cd MDT_Gromoacs_Lysozyme` and run the script `./run_gmx`.

1.  Clone this repository: `git clone https://gitlab.eps.surrey.ac.uk/mw00368/MDT_Gromacs1_Lysozyme.git`
2.  Go onto the debug node: `ssh node41`
3.  Change directory: `cd MDT_Gromacs1_Lysozyme`
4.  Run the script: `./run_gmx.sh`

To submit the script using SLURM run submit.sh:

`./submit.sh`

run_gmx.sh will move the contents of the working directory to a subdirectory parallel_scratch named after the SLURM job ID. Once the calculations finish, this directory is moved back to the working directory.

The log files $JOB_ID.o and $JOB_ID.e will be in the working directory until completion, at which point they are put in the sub-directory of the associated job. You may find it useful to view the results using the results.sh script of [MShTools](#https://github.com/mwinokan/MShTools).

